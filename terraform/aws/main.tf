resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCJH+iiqFULAXZ1RuXs/hqK6lLAumyunT1WJ2CpVlEUQEct7lh3pCd9WQlst6PTUxoiKBv29/JfJx5ot/Z767LbqFziMXtcFi9Tc5clsIZ7cA2bSMXOCvkWlJco9VHNbpPFqZtHjIE2y8BL26qTmnehVGEZX5W75+PkcbqGac2YIgG2ELaOzVXemPN8BAOlPZKJiK9LWxTj6AapROlbjYIdIOtL5l/aooL7l1VYh86iwb5AsrHyBrI9RffcBIgxuRXdiRF6xAynJMEtyf34SLWuiZX6MXh/uT2WJjCYwIsXu3zK3vFaa2ZE20hJbHrBbTR/ApjHieC9KwHfapPGH4aqaIFUa6fYQwSCYLzh9T1EDgmq7xB0lXqk1l9Wi81Z0QpcY/kXfeDHjbz15H20BC5JodyJSgVKl39ZEw3EkUywSkZluca/ZnYyXNW+24n+T9QcjaP4+PynLHqFQWiNHgpqZ650R/x3texYMERmY3WNDdfqRvm41pk1qCvIdpVLdfc= ronin@msi"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id =aws_default_vpc.default.id

  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 5000
    to_port     = 5000
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

provider "aws" {
  region = "eu-north-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["631658917729"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name      = "id_rsa"

  tags = {
    Name = "HelloWorld"
  }
}
